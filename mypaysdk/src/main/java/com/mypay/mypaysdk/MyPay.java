package com.mypay.mypaysdk;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;


public class MyPay {
    private final static MyPay _instance = new MyPay();

    private Context _context;
    private String _apiToken;

    private MyPay() {
    }

    public static MyPay getInstance() {
        return _instance;
    }

    public void initialize(Context context, String apiToken) {
        _context = context.getApplicationContext();
        _apiToken = apiToken;
    }

    public void pay(@Nullable String memberId) {
        ensureIsInitialized();
        Intent intent = new Intent(_context, MyPayActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        intent.putExtra("memberId", memberId);
        _context.startActivity(intent);
    }

    public void dismiss() {
        if (_context != null) {
            _context.sendBroadcast(new Intent(MyPayIntent.PAYMENT_DISMISSED));
        }
    }

    private void ensureIsInitialized() {
        if (_context == null) {
            throw new IllegalStateException("MyPay has not been initialized yet!");
        }
    }

    String getApiToken() {
        return _apiToken;
    }
}
