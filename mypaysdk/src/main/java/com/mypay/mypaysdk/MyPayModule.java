package com.mypay.mypaysdk;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import javax.annotation.Nonnull;

public class MyPayModule extends ReactContextBaseJavaModule {
    public MyPayModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Nonnull
    @Override
    public String getName() {
        return "MyPay";
    }

    @ReactMethod
    public void dismiss() {
        MyPay.getInstance().dismiss();
    }
}