import React, {Component} from 'react'
import {App as MyPay, AppProps} from 'mypay-app/src';


export default class App extends Component<AppProps> {
  render() {
    return <MyPay
      headerImageUri='https://s3.eu-central-1.amazonaws.com/my-pay-static/sp-logo.png'
      primaryColor='#ffd53f'
      backgroundColor='#f9f9f9'
      secondaryColor='#459ad4'
      {...this.props}/>;
  }
}
